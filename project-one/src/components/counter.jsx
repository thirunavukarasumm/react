import React, { Component } from "react";

class Counter extends Component {
  constructor() {
    super();
    this.state = {
      count: 0,
      tags: [],
      // imageUrl:"https://picsum.photos/200"
    };
    // this.handleIncrement = this.handleIncrement.bind(this)
  }
  //   styles = {
  //     fontSize:10,
  //     fontWeight:bold
  //   }
  //   renderTags(){
  //     if(this.state.tags.length === 0){
  //         return(<p>There are no items!</p>);
  //     } else {
  //         return (
  //           <ul>
  //             {this.state.tags.map((tag) => {
  //               return <li key={tag}>{tag}</li>;
  //             })}
  //           </ul>
  //         );
  //     }
  //   }
  render() {
    return (
      <React.Fragment>
        {/* <img src={this.state.imageUrl}/> */}
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={()=>{this.handleIncrement({id:1})}}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
        {/* {this.state.tags.length === 0 && "Error occurred!"}
        {this.renderTags()} */}
      </React.Fragment>
    );
  }
  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.state.count === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { count } = this.state;
    return count === 0 ? "Zero" : count;
  }

  handleIncrement = (event) => {
    console.log(event);
    console.log("Clicked!");
    this.setState({
        count:this.state.count + 1
    })
  };
}

export default Counter;

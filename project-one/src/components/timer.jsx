import React, { Component } from "react";
import RenderTimer from "./renderTimer";
class Timer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      time: new Date().toLocaleTimeString(),
    };
  }
  componentDidMount() {
    this.intervalID = setInterval(() => this.updateClock(), 1000);
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  updateClock() {
    this.setState({
      time: new Date().toLocaleTimeString(),
    });
  }
  render() {
    return (
      <div className="Time">
        {/* <p> {this.state.time}</p> */}
        <RenderTimer timer = {this.state.time}/>
      </div>
    );
  }
}
export default Timer;

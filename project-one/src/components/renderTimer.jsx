import React, { Component } from 'react';

class RenderTimer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      timer: this.props.timer,
    };
  }
  componentDidUpdate() {
    this.intervalID = setInterval(
      () =>
        this.setState({
          timer: this.props.timer,
        }),
      1000
    );
  }
  componentWillUnmount() {
    clearInterval(this.intervalID);
  }

  render() {
    return (
      <>
        <p>{this.state.timer}</p>
        <p>{this.props.timer}</p>
      </>
    );
  }
}
 
export default RenderTimer;
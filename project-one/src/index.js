// React import
import React from "react";
import ReactDOM from 'react-dom/client';

// Bootstrap
import "bootstrap/dist/css/bootstrap.css"

// Components
import Counter from "./components/counter"
import Timer from "./components/timer"





const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <Timer/>
);


/* 

Props are immutable
function Welcome(props) {
    props.name = "thiru"
    return <h1>Hello, {props.name}</h1>;
}

const root = ReactDOM.createRoot(document.getElementById('root'));
const element = <Welcome name="Sara" />;
root.render(element);

*/